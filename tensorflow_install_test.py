# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 14:20:00 2018

Test that Tensorflow and Keras work

To choose between CPU and GPU
  https://stackoverflow.com/questions/40690598/can-keras-with-tensorflow-backend-be-forced-to-use-cpu-or-gpu-at-will

Using GPU
  https://www.tensorflow.org/programmers_guide/using_gpu
  "If a TensorFlow operation has both CPU and GPU implementations, the GPU devices will 
  be given priority when the operation is assigned to a device." 

@author: mikko hakala
"""

# Apparently not needed
# https://stackoverflow.com/questions/37893755/tensorflow-set-cuda-visible-devices-within-jupyter
#import os
#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
#os.environ["CUDA_VISIBLE_DEVICES"]="0"

import tensorflow as tf
#from sklearn import svm




def get_available_gpus_cpus():
    # Not used
    from tensorflow.python.client import device_lib
    local_device_protos = device_lib.list_local_devices()
    gpus = [x.name for x in local_device_protos if x.device_type == 'GPU']
    cpus = [x.name for x in local_device_protos if x.device_type == 'CPU']
    print('gpus and cpus available:')
    return("gpus: " + str(gpus) + " cpus: " + str(cpus))






def test_cpu():
    with tf.device('/cpu:0'):
        a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
        b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
        c = tf.matmul(a, b)
        # Creates a session with log_device_placement set to True.
        sess = tf.Session(config=tf.ConfigProto(log_device_placement=True, allow_soft_placement = True))
        # sess = tf.Session()

        # Runs the op.
        print('Tensorflow matmul a*b')
        print("CPU:", sess.run(c))


def test_gpu():
    with tf.device('/gpu:0'):
        a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
        b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
        c = tf.matmul(a, b)
        # Creates a session with log_device_placement set to True.
        sess = tf.Session(config=tf.ConfigProto(log_device_placement=True, allow_soft_placement = True))
        # Runs the op.
        print('Tensorflow matmul a*b')
        print("GPU:", sess.run(c))


def tf_first_test():
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
    c = tf.matmul(a, b)
    sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    print('Simple tensorflow matmul a*b')
    print(sess.run(c))




def keras_first_test(device):
    # https://machinelearningmastery.com/tutorial-first-neural-network-python-keras/

    from keras.models import Sequential
    from keras.layers import Dense
    import numpy
    # fix random seed for reproducibility
    numpy.random.seed(7)

    print('Keras quick test')
    print('Device:', device)


    # load pima indians dataset
    dataset = numpy.loadtxt("pima-indians-diabetes.csv", delimiter=",")
    # split into input (X) and output (Y) variables
    X = dataset[:,0:8]
    Y = dataset[:,8]

    #with tf.device('/cpu:0'):
    with tf.device(device):
    #if(True):
        # create model
        model = Sequential()
        model.add(Dense(12, input_dim=8, activation='relu'))
        model.add(Dense(8, activation='relu'))
        model.add(Dense(1, activation='sigmoid'))
        # Compile model
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        # Fit the model
        model.fit(X, Y, epochs=2, batch_size=10)  # was: 150
        
    # evaluate the model
    scores = model.evaluate(X, Y)
    print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))



def main(): 

    #print(get_available_gpus_cpus())
    #input("press enter")
    
    from tensorflow.python.client import device_lib
    print('Local devices:', device_lib.list_local_devices())
    print('====')

    test_cpu()
    test_gpu()
    #input("press enter")


    tf_first_test()
    #input("press enter")



    #device = '/cpu:0'
#    device = '/device:GPU:0'
    device = '/gpu:0'    
    keras_first_test(device)

    print('====')
    print('Local devices:', device_lib.list_local_devices())
    
    

    print('Done')


if __name__ == '__main__': 
     main() 





